package test.java.br.com.caelum.blog.jenkins;
import static org.junit.Assert.assertEquals;
import main.java.br.com.caelum.blog.jenkins.Imposto;
import main.java.br.com.caelum.blog.jenkins.Orcamento;

import org.junit.Test;

public class ImpostoTest {

	@Test
	public void deveCalcularImpostoDeDezPorCento() {
		Orcamento orcamento = new Orcamento(100.0);
		Imposto imposto = new Imposto();
		assertEquals(10.0, imposto.calcula(orcamento), Double.MIN_VALUE);
	}
}
